import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { timestamp } from 'rxjs/operators';
import { ValidadoresService } from '../../services/validadores.service';

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.css']
})
export class ReactiveComponent implements OnInit {

  forma: FormGroup;

  constructor( private formBuilder: FormBuilder,
               private validador: ValidadoresService ) {
    this.createFormulario();
    this.cargarDataAlFormulario();
    this.crearListeners();
   }

  ngOnInit(): void {
  }

  get nombreNoValida() {
    return this.forma.get('nombre').invalid && this.forma.get('nombre').touched;
  }

  get pasatiempos() {
    return this.forma.get('pasatiempos') as FormArray;
  }

  passDiferentes(): boolean {
    const pass1: string = this.forma.get('pass1').value;
    const pass2: string = this.forma.get('pass2').value;

    // console.log('pass1: ' + pass1);
    // console.log('pass2: ' + pass2);

    if ( pass1?.length < 1 ) { return true; }
    if ( pass2?.length < 1 ) { return true; }

    return ( pass1 === pass2) ? false : true;
  }

  campoNoValido(campo: string): boolean {
    return this.forma.get(campo).invalid && this.forma.get(campo).touched;
  }

  createFormulario() {
    this.forma = this.formBuilder.group( {
      nombre: ['', [Validators.required, Validators.minLength(5) ]], //[ valor por defecto, validaciones sincronos, validadores asincronos]
      apellido: ['', [Validators.required, this.validador.noApellido]],
      correo: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      usuario: ['', Validators.required, this.validador.existeUsuario],
      pass1: ['', Validators.required],
      pass2: ['', Validators.required],
      direccion: this.formBuilder.group({
        distrito: ['', Validators.required],
        ciudad: ['', Validators.required]
      }),
      pasatiempos: this.formBuilder.array([])
    }, {
      validators: this.validador.passIguales('pass1', 'pass2') // esto es para marcar el error a nivel de formulario
    });
  }

  crearListeners() {
    // this.forma.valueChanges.subscribe( valor => console.log(valor)); // todos los campos
    // this.forma.statusChanges.subscribe( status => console.log(status)); // el estado del formulario
    this.forma.get('nombre').valueChanges.subscribe( valor => console.log(valor)); // solo un campo
  }

  cargarDataAlFormulario() {

    // setvalue exige datos en todos los datos
    // reset no, solo algunos o todos. Utilizado para los select del formulario.
    // this.forma.setValue({
      this.forma.reset({
    nombre: 'Carlitos ^^',
    apellido: 'Coaquira ^^',
    correo: 'carlos@yahoo.com',
    direccion: {
    distrito: 'Lima',
    ciudad: 'Lima'
    }
    });
  }

  agregarPasatiempo() {
    // this.forma.get('pasatiempos')
    this.pasatiempos.push( this.formBuilder.control(''));
  }

  borrarPasatiempo( i: number) {
    this.pasatiempos.removeAt(i);
  }

  guardar() {
    if (this.forma.invalid) {
      Object.values ( this.forma.controls).forEach ( control => {

        if (control instanceof FormGroup) {
          Object.values(control.controls).forEach ( control => control.markAsTouched());
        }


        control.markAsTouched();
      });
      return;
    }

    console.log(this.forma);

    this.forma.reset({
      nombre: 'sin nombre'
    });


  }

  //Posteo de información
  // this.forma.reset({
  //  nombre: 'sin nombre'
  // });


}
